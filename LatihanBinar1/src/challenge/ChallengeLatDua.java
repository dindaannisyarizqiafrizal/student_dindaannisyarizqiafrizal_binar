package challenge;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class ChallengeLatDua {
	
	static Scanner in= new Scanner(System.in);
	public static void main(String[] args) throws IOException {    
	
		String ulang = "y";
        
        while (ulang.equals("y")){
	    	ArrayList<Integer>data = readfile(); 
	    	int pil;
	    
	    	System.out.println();
	        System.out.println("-------------------------------------");
	        System.out.println("APLIKASI PENGOLAH NILAI SISWA");
	        System.out.println("-------------------------------------");
	        System.out.println("Letakkan file csv dengan nama file data_Sekolah di direktori C://temp//direktori");
	        System.out.println("Pilih Menu: ");
	        System.out.println("1. Generate txt untuk menampilkan modus");
	        System.out.println("2. Generate txt untuk menampilkan nilai rata-rata, median");
	        System.out.println("3. Generate kedua file");
	        System.out.println("0. Exit");
	        System.out.println("Masukkan Pilihan: ");
	        pil = in.nextInt();
	        
	        switch (pil) {
	            case 1 :
	                String valueofmodus = String.valueOf(modus(data));
	                
	                try{
	                	
	                	FileWriter fw = new FileWriter("C:\\Users\\arif rahman\\Documents\\binar\\student_dindaannisyarizqiafrizal_binar\\LatihanBinar1\\src\\challenge\\modus.txt");  
	                	BufferedWriter bw = new BufferedWriter(fw);
	                	bw.write("Modus : "+valueofmodus);
	                	bw.newLine();
	                	bw.flush();
	                	bw.close();
	                } catch (IOException e) {
	                	e.printStackTrace();
	                }
	                System.out.println("Modus  "+valueofmodus);
	                System.out.println();
	               
	                break;
	                
	            case 2 :
	            	String valueofmean = String.valueOf(mean(data));
	            	String valueofmedian = String.valueOf(median(data));
	                
	                try{
	                	FileWriter fw = new FileWriter("C:\\Users\\arif rahman\\Documents\\binar\\student_dindaannisyarizqiafrizal_binar\\LatihanBinar1\\src\\challenge\\mean_median.txt");
	                	BufferedWriter bw = new BufferedWriter(fw);
	                	bw.write("Mean "+valueofmean);
	                	bw.newLine();
	                	bw.write("Median "+valueofmedian);
	                	bw.newLine();
	                	bw.flush();
	                	bw.close();
	                } catch (IOException e) {
	                	e.printStackTrace();
	                }
	                System.out.println("Mean "+valueofmean);
	                System.out.println("Median "+valueofmedian);
	                System.out.println();
	               
	                break;
	                
	            case 3 :
	            	String valueofmean1 = String.valueOf(mean(data));
	            	String valueofmedian1 = String.valueOf(median(data));
	            	String valueofmodus1 = String.valueOf(modus(data));
	                
	                try{
	                	FileWriter fw = new FileWriter("C:\\Users\\arif rahman\\Documents\\binar\\student_dindaannisyarizqiafrizal_binar\\LatihanBinar1\\src\\challenge\\mean_median_modus.txt");
	                	BufferedWriter bw = new BufferedWriter(fw);
	                	bw.write("----------------------------------");
	                	bw.newLine();
	                	bw.write("  Berikut Hasil Pengolahan Nilai  ");
	                	bw.newLine();
	                	bw.write("----------------------------------");
	                	bw.newLine();
	                	bw.write("Mean "+valueofmean1);
	                	bw.newLine();
	            		bw.write("Median "+valueofmedian1);
	            		bw.newLine();
	            		bw.write("Modus "+valueofmodus1);
	                	bw.newLine();
	                	bw.flush();
	                	bw.close();
	                } catch (IOException e) {
	                	e.printStackTrace();
	                }
	                System.out.println("Mean "+valueofmean1);
	                System.out.println("Median "+valueofmedian1);
	                System.out.println("Modus "+valueofmodus1);
	                System.out.println();
	                

	                break;  
	                
	            	
	            case 0:
	            	System.out.println("Terima Kasih!!");
	            	System.exit(0);
	            	break;
	            	
	            default:
	                System.out.println("Pilihan tidak tersedia!");
	        		}
	    		}   
			}
		public static ArrayList<Integer> readfile(){
	    	String data = "C:\\Users\\arif rahman\\Documents\\binar\\student_dindaannisyarizqiafrizal_binar\\LatihanBinar1\\src\\challenge\\data_sekolah.csv";
	    	ArrayList<Integer> sd = new ArrayList<Integer>();
	    	
	    	try {
	    		File file = new File(data);
	    		FileReader fr = new FileReader(file);
	    		BufferedReader br = new BufferedReader(fr);
	    		String line = "";
	    		String[] tempArr;
	    		
	    		while((line = br.readLine()) != null) {
	    			tempArr = line.split(";");
	    			for(String s : tempArr) {
	    				if(s.equals(tempArr[0]));
	    				else {
	    					sd.add(Integer.valueOf(s));
	    				}
	    			}
	    		}
	    		
	    		br.close();
	    	} catch (IOException e) {
	    	}
	    	return sd;
	    }
	    
	    public static float mean(ArrayList<Integer>mn) {
	    	
	    	float mean = 0;
	    	int size = mn.size();
	    	for(float j : mn) {
	    		mean = mean + j;
	    	}
	    	
	    	float hasil = mean/size;
	    	return hasil;
	    }
	    
	    public static float median(ArrayList<Integer>md) {
	    	
	    	float median = 0;
	    	int size = md.size();
	    	if(size%2 == 0) {
	    		float x1 = md.get(size/2 - 1);
	    		float x2 = md.get(size/2);
	    		median = (x1+x2)/2;
	    	} else {
	    		median = md.get(size/2);
	    	}
	    	
	    	return median;
	    }
	    
	    public static float modus(ArrayList<Integer>modus) {
	    	
	    	Integer[] IntArray = null;
	    	IntArray = new Integer[modus.size()];
	    	for(int i = 0; i < modus.size(); i++) {
	    		IntArray[i] = modus.get(i);
	    	}
	    	int maxkey = 0;
	    	int maxcount = 0;
	    	
	    	int[] counts = new int[modus.size()];
	    	for(int i = 0; i < IntArray.length; i++) {
	    		counts[IntArray[i]]++;
	    		if(maxcount < counts[IntArray[i]]) {
	    			maxcount = counts[IntArray[i]];
	    			maxkey = IntArray[i];
	    		}
	    	}
	    	return maxkey;
	    }
}