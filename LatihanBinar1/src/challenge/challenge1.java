package challenge;
import java.util.Scanner;

public class challenge1 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        double phi = 3.14;
        double sisi, jari, alas, tinggi, panjang, lebar;
        double luaspersg, luaslingkr, luassgtg, luasppjn;
        double sisi1, pjng, leb, tg1, tg2, jar2;
        double volumekubus, volumebalok, volumetabung;
        String ulang = "y";
        
        while (ulang.equals("y")){

            System.out.println("------------------------------------");
            System.out.println("Kalkukalor Penghitung Luas dan Volum");
            System.out.println("------------------------------------");
            System.out.println("Menu");
            System.out.println("1. Hitung Luas Bidang");
            System.out.println("2. Hitung Volum");
            System.out.println("0. Tutup Aplikasi");
            System.out.println("------------------------------------");
            System.out.println("Masukan pilihan anda : ");
            int pil = input.nextInt();
            
            switch (pil) {
                case 1:
                    System.out.println("------------------------------------");
                    System.out.println("   Pilih bidang yang akan dihitung  ");
                    System.out.println("------------------------------------");
                    System.out.println("1. Persegi");
                    System.out.println("2. Lingkaran");
                    System.out.println("3. Segitiga");
                    System.out.println("4. Persegi Panjang");
                    System.out.println("0. Kembali ke menu sebelumnya");
                    System.out.println("------------------------------------");
                    System.out.println("Anda memilih : ");
                    int pil2 = input.nextInt();

                    System.out.println();

                    if (pil2 == 1) {
                            System.out.println("-------------------------");
                            System.out.println("   Anda memilih Persegi  ");
                            System.out.println("-------------------------");
                            System.out.println("Masukan sisi : ");
                            sisi = input.nextDouble();
                           
                            luaspersg = sisi * sisi;

                            System.out.println("processing");
                            System.out.println("Luas dari persegi adalah " + luaspersg);
                    }

                    else if (pil2 == 2) {
                            System.out.println("----------------------------");
                            System.out.println("   Anda memilih Lingkaran   ");
                            System.out.println("----------------------------");
                            System.out.println("Masukan jari-jari : ");
                            jari = input.nextDouble();

                            luaslingkr = phi * jari * jari;
                            
                            System.out.println("processing");
                            System.out.println("Luas dari lingkaran adalah " + luaslingkr);
                    }

                    else if (pil2 == 3) {
                            System.out.println("---------------------------");
                            System.out.println("   Anda memilih Segitiga   ");
                            System.out.println("---------------------------");
                            System.out.println("Masukan alas   : ");
                            alas = input.nextDouble();
                            System.out.println("Masukan tinggi : ");
                            tinggi = input.nextDouble();

                            luassgtg = (alas * tinggi) / 2;

                            System.out.println("processing");
                            System.out.println("Luas dari lingkaran adalah " + luassgtg);
                    }

                    else if (pil2 == 4) {
                            System.out.println("----------------------------------");
                            System.out.println("   Anda memilih Persegi Panjang   ");
                            System.out.println("----------------------------------");
                            System.out.println("Masukan panjang : ");
                            panjang = input.nextDouble();
                            System.out.println("Masukan lebar   : ");
                            lebar = input.nextDouble();

                            luasppjn = panjang * lebar;

                            System.out.println("processing");
                            System.out.println("Luas dari lingkaran adalah " + luasppjn);
                    }

                    else if (pil2 == 0) {
                            System.out.println("Apakah anda ingin kembali ke menu (y/n) ?");
                            ulang = input.next();
                           
                    }
                    break;

                case 2:
                    System.out.println("------------------------------------");
                    System.out.println("           Mencari Volum            ");
                    System.out.println("------------------------------------");
                    System.out.println("1. Volum Kubus");
                    System.out.println("2. Volum Balok");
                    System.out.println("3. Volum Tabung");
                    System.out.println("0. Kembali ke menu sebelumnya");
                    System.out.println("------------------------------------");
                    System.out.println("Masukan pilihan anda : ");
                    int pil3 = input.nextInt();

                    System.out.println();

                    if (pil3 == 1) {
                            System.out.println("-------------------------");
                            System.out.println("    Anda memilih Kubus   ");
                            System.out.println("-------------------------");
                            System.out.println("Masukan sisi kubus : ");
                            sisi1 = input.nextDouble();

                            volumekubus = sisi1 * sisi1 * sisi1;
                            
                            System.out.println("processing");
                            System.out.println("Volume dari kubus adalah " + volumekubus);
                    }

                    else if (pil3 == 2) {
                            System.out.println("-------------------------");
                            System.out.println("    Anda memilih Balok   ");
                            System.out.println("-------------------------");
                            System.out.println("Masukan panjang balok : ");
                            pjng = input.nextDouble();
                            System.out.println("Masukan lebar balok: ");
                            leb = input.nextDouble();
                            System.out.println("Masukan tinggi balok : ");
                            tg1 = input.nextDouble();

                            volumebalok = pjng * leb * tg1;

                            System.out.println("processing");
                            System.out.println("Volume dari balok adalah " + volumebalok);
                    }

                    else if (pil3 == 3) {
                            System.out.println("-------------------------");
                            System.out.println("   Anda memilih Tabung   ");
                            System.out.println("-------------------------");
                            System.out.println("Masukan jari-jari tabung : ");
                            jar2 = input.nextDouble();
                            System.out.println("Masukan tinggi tabung : ");
                            tg2 = input.nextDouble();

                            volumetabung = phi * tg2 * jar2;

                            System.out.println("processing");
                            System.out.println("Volume dari tabung adalah " + volumetabung);
                    }

                    else if (pil3 == 0) {
                            System.out.println("Apakah anda ingin kembali ke menu (y/n) ?");
                            ulang = input.next();
                    }
                    break;
                case 0:
                	System.out.println("Terima Kasih");
        		    System.exit(0);
                    break; 
            }
        }
    }
}
