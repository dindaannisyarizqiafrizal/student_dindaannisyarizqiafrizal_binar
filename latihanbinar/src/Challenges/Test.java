package Challenges;

import static org.junit.jupiter.api.Assertions.*;

import java.text.DecimalFormat;
import java.util.ArrayList;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class Test {
	@Test
	@DisplayName("Positive Tes - Successfull Mean")
	void testAdditionSuccess() {
		ArrayList<Integer> mean = new ArrayList<>(Arrays.asList(7, 8, 7, 8, 9, 5, 10, 9, 9));	
		Nilai keseluruhan = new Nilai(mean);
		double hasil = keseluruhan.getmean();
		Assertions.assertEquals(8.0, hasil);
	}
	@Test
	@DisplayName("Positive Tes - Successfull Modus")
	void testModus() {
		ArrayList<Integer> modus = new ArrayList<>(Arrays.asList(7, 8, 7, 8, 9, 5, 10, 9, 9));
		Nilai keseluruhan = new Nilai(modus);
		double hasil = keseluruhan.getmodus();
		Assertions.assertEquals(9, hasil);
	
		
	}
	@Test
	@DisplayName("Positive Tes - Successfull Median")
	void testMedian() {
		ArrayList<Integer> median = new ArrayList<>(Arrays.asList(7, 8, 7, 8, 9, 5, 10, 9, 9));
		Nilai keseluruhan = new Nilai(median);
		double hasil = keseluruhan.getmedian();
		Assertions.assertEquals(9.0 , hasil);
		
		
	}
	@Test
	@DisplayName("Positive Tes - Successfull Frekuensi")
	void testFrekuensi() {
		ArrayList<Integer> frekuensi = new ArrayList<>(Arrays.asList(7, 8, 7, 8, 9, 5, 10, 9, 9));
		Nilai keseluruhan = new Nilai(frekuensi);
		double hasil = keseluruhan.getfrekuensi(7,7);
		Assertions.assertEquals(2, hasil);

	}

}