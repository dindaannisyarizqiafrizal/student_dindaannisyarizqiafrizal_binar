package Challenges;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class PositiveTest {

	@Test
	@DisplayName("Positive Test Mean")
	void testAdditionSuccess() {
		ArrayList<Integer> mean = new ArrayList<>(Arrays.asList(7, 8, 7, 8, 9, 5, 10, 9, 9));	
		Nilai keseluruhan = new Nilai(mean);
		double hasil = keseluruhan.mean();
		Assertions.assertEquals(8.0, hasil);
	}
	
	@Test
	@DisplayName("Positive Test Modus")
	void testModus() {
		ArrayList<Integer> modus = new ArrayList<>(Arrays.asList(7, 8, 7, 8, 9, 5, 10, 9, 9));
		Collections.sort(modus);
		Nilai keseluruhan = new Nilai(modus);
		double hasil = keseluruhan.modus();
		Assertions.assertEquals(9, hasil);	
	}

	@Test
	@DisplayName("Positive Test Median")
	void testMedian() {
		ArrayList<Integer> median = new ArrayList<>(Arrays.asList(7, 8, 7, 8, 9, 5, 10, 9, 9));
		Collections.sort(median);
		Nilai keseluruhan = new Nilai(median);
		double hasil = keseluruhan.median();
		Assertions.assertEquals(8.0, hasil);		
	}

	@Test
	@DisplayName("Positive Test Frekuensi")
	void testFrekuensi() {
		ArrayList<Integer> frekuensi = new ArrayList<>(Arrays.asList(7, 8, 7, 8, 9, 5, 10, 9, 9));
		Nilai keseluruhan = new Nilai(frekuensi);
		double hasil = keseluruhan.frekuensi(7,7);
		Assertions.assertEquals(2, hasil);
	}
}
